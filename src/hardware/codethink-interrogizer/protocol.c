/*
 * This file is part of the libsigrok project.
 *
 * Copyright (C) 2020 Tom Eccles <tom.eccles@codethink.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "protocol.h"

#define IO_TIMEOUT_MS 100

/* valid sample rates */
const uint64_t samplerates[NUM_SAMPLE_RATES] = {
	SR_KHZ(200),
	SR_KHZ(100),
	SR_KHZ(50),
	SR_KHZ(20),
	SR_KHZ(10),
	SR_KHZ(5),
	SR_KHZ(2),
	SR_KHZ(1),
	SR_HZ(500),
	SR_HZ(200),
	SR_HZ(100),
	SR_HZ(50),
	SR_HZ(20),
	SR_HZ(10),
	SR_HZ(5),
	SR_HZ(2),
	SR_HZ(1),
};

static uint8_t get_counter(uint8_t *counter)
{
	uint8_t ret = *counter;

	if (*counter == UINT8_MAX) {
		*counter = 0;
	} else {
		*counter += 1;
	}

	return ret;
}

/* reads a single sample packet from serial */
static int ct_receive_data_packet(struct dev_context *devc,
		struct sr_serial_dev_inst *serial)
{
	uint8_t *sample_buffer;
	uint8_t expected_counter;
	int n_read;
	GQueue *packet_queue;

	sample_buffer = g_try_malloc(MAX_PACKET_LEN);
	if (!sample_buffer) {
		return SR_ERR_MALLOC;
	}

	n_read = serial_read_blocking(serial, sample_buffer,
			MAX_PACKET_LEN, IO_TIMEOUT_MS);
	if (n_read != MAX_PACKET_LEN) {
		sr_warn("Read error: %i/%i", n_read, MAX_PACKET_LEN);
		goto err;
	}

	expected_counter = get_counter(&devc->recv_counter);
	if (sample_buffer[PACKET_COUNTER] != expected_counter) {
		sr_warn("Packet counter mismatch: expected %u, got %u\n",
				(unsigned) expected_counter,
				(unsigned) sample_buffer[PACKET_COUNTER]);
	}

	switch (sample_buffer[PACKET_OPCODE]) {
	case PORTA_SAMPLES:
		packet_queue = devc->porta_queue;
		break;
	case PORTB_SAMPLES:
		packet_queue = devc->portb_queue;
		break;
	default:
		sr_warn("Unknown packet type 0x%x", (int) sample_buffer[PACKET_OPCODE]);
		goto err;
	}

	g_queue_push_tail(packet_queue, sample_buffer);
	return SR_OK;

err:
	g_free(sample_buffer);
	return SR_ERR;
}

/*
 * Put everything in one buffer in the order sigrok wants. This means
 * within each unitsize-bytes block of the output buffer, the sample for each
 * channel is given at an offset equal to the channel's index.
 * For reference see src/output/bits.c receive().
 *
 * out should be NUM_SAMPLES * unitsize long.
 * Only enabled channels will be read from so if a whole port is disabled, that
 * port buffer won't be read from and so can be NULL.
 */
static void ct_format_samples(const uint8_t porta[NUM_SAMPLES],
			const uint8_t portb[NUM_SAMPLES],
			uint8_t * restrict out,
			const uint16_t unitsize,
			struct sr_dev_inst *sdi)
{
	unsigned i;
	GSList *channels;
	struct sr_channel *channel;
	int idx;
	uint8_t *p;
	uint8_t is_set;
	const uint8_t * restrict input_buffer;

	for (i = 0; i < NUM_SAMPLES * unitsize; i += unitsize) {
		for (channels = sdi->channels; channels; channels = channels->next) {
			channel = channels->data;

			if (!channel->enabled) {
				continue;
			}

			idx = channel->index;
			if (idx < 8) {
				/* B has lower indices because it is listed
				   first on the PCB */
				input_buffer = portb;
			} else {
				input_buffer = porta;
			}

			is_set = input_buffer[i / unitsize] & (1 << (idx % 8));

			/* find the right byte in the output buffer */
			p = out + i + idx/8;

			/* set or clear this bit in the output buffer */
			if (is_set) {
				*p |= (1 << (idx % 8));
			} else {
				*p &= ~(1 << (idx % 8));
			}
		}
	}
}

/**
 * Callback for data acquisition events.
 *
 * @param[in] fd (unused)
 * @param[in] revents indicates what kind of IO event happened
 * @param[in] cb_data callback data - we use this to store the struct sr_dev_inst
 *
 * @retval TRUE everything is fine
 * @retfal FALSE everything is not fine
 *
 * @private
 */
SR_PRIV int ct_receive_data(int fd, int revents, void *cb_data)
{
	struct sr_dev_inst *sdi;
	struct dev_context *devc;

	uint8_t *porta_buffer;
	uint8_t *portb_buffer;
	gsize num_for_frontend;
	uint8_t *dual_buffer;

	/*
	 * 2 8-bit ports: 2 bytes per sample
	 *
	 * In theory we could vary this e.g. if only channels from one port are
	 * enabled then only send bytes for that port. In practice, only the C
	 * output modules understand that. Pulseview gets confused.
	 */
	const uint16_t unitsize = 2;

	struct sr_datafeed_packet packet;
	struct sr_datafeed_logic logic;
	struct sr_serial_dev_inst *serial;

	(void)fd;

	if (!(sdi = cb_data)) {
		sr_warn("%s: !cb_data", __func__);
		return TRUE;
	}

	if (!(devc = sdi->priv)) {
		sr_warn("%s: !sdi->priv", __func__);
		return TRUE;
	}

	serial = sdi->conn;

	/* if we should be acquiring samples and there are some pending */
	if (revents == G_IO_IN && devc->num_samples < devc->limit_samples) {
		/* while we don't have at least one packet available for each port */
		do {
			if (ct_receive_data_packet(devc, serial) != SR_OK) {
				return FALSE;
			}
		} while ((devc->porta_mask && g_queue_is_empty(devc->porta_queue)) ||
			 (devc->portb_mask && g_queue_is_empty(devc->portb_queue)));

		porta_buffer = g_queue_pop_head(devc->porta_queue);
		if (devc->porta_mask) {
			g_assert(porta_buffer);
		}

		portb_buffer = g_queue_pop_head(devc->portb_queue);
		if (devc->portb_mask) {
			g_assert(portb_buffer);
		}

		dual_buffer = g_try_malloc(NUM_SAMPLES * unitsize);
		if (!dual_buffer) {
			g_free(porta_buffer);
			g_free(portb_buffer);
			sr_warn("ENOMEM");
			return FALSE;
		}

		ct_format_samples(porta_buffer + PACKET_DATA_START,
				portb_buffer + PACKET_DATA_START, dual_buffer,
				unitsize, sdi);

		g_free(porta_buffer);
		g_free(portb_buffer);

		/*
		 * Interrogizer will only send us units of NUM_SAMPLES,
		 * the sample limit may not be a multiple of this. If so, don't
		 * tell the frontend about the excess samples.
		 *
		 * For example, if the sample limit is 200 samples and
		 * NUM_SAMPLES is 63, we will give the full NUM_SAMPLES for the
		 * first three rounds, leaiving devc->num_samples at 189. Then
		 * on this final fourth iteration we only send upstream 200 - 189
		 * = 11 samples, so that the final number that the frontend sees
		 * is 200.
		 */
		if ((devc->num_samples + NUM_SAMPLES) >= devc->limit_samples) {
			num_for_frontend = devc->limit_samples - devc->num_samples;
		} else {
			num_for_frontend = NUM_SAMPLES;
		}
		devc->num_samples += NUM_SAMPLES;

		/* send this on to the frontend */
		packet.type = SR_DF_LOGIC;
		packet.payload = &logic;
		logic.unitsize = unitsize;
		logic.data = dual_buffer;
		logic.length = num_for_frontend * unitsize;
		if (sr_session_send(sdi, &packet) != SR_OK) {
			g_free(dual_buffer);
			sr_warn("sr_session_send failed");
			return FALSE;
		}
		g_free(dual_buffer);
	} else {
		/* This is the main loop telling us a timeout was reached,
		 * or we-ve acquired all the samples we asked for -- we're
		 * done.
		 */
		if (devc->num_samples < devc->limit_samples) {
			sr_err("Timeout fetching samples from device");
		}

		if (ct_acquisition_stop(sdi) != SR_OK) {
			sr_warn("failed to stop acquisition");
			return FALSE;
		}
	}

	return TRUE;
}

/* send a command to the device */
static int ct_cmd(struct dev_context *devc, struct sr_serial_dev_inst *serial,
		char cmd[], size_t len)
{
	int ret;
	uint8_t response;
	uint8_t junk_buffer[NUM_SAMPLES];
	uint8_t buf[MAX_PACKET_LEN];

	if (len == 0) {
		return SR_OK;
	}
	if (len > MAX_PACKET_LEN - 1) {
		/* too big to fit the counter byte */
		return SR_ERR;
	}

	sr_spew("Sending 0x%x [...]", cmd[0]);

	if (cmd[PACKET_OPCODE] == CMD_RESET) {
		devc->send_counter = 0;
		devc->recv_counter = 0;
	}

	buf[PACKET_OPCODE] = cmd[PACKET_OPCODE];
	buf[PACKET_COUNTER] = get_counter(&devc->send_counter);
	for (unsigned i = PACKET_DATA_START; i < len + 1; i++) {
		buf[i] = cmd[i - 1];
	}

	ret = serial_write_blocking(serial, buf, len + 1, IO_TIMEOUT_MS);
	if ((size_t) ret != len + 1) {
		sr_warn("Error writing to serial: %i", ret);
		/* ret == 0 is a timeout, otherwise ret is some error */
		return (ret == 0) ? SR_ERR : ret;
	}

	while (TRUE) {
		ret = serial_read_blocking(serial, &response, sizeof(response),
					   IO_TIMEOUT_MS);
		if (ret != 1) {
			sr_warn("Error reading from serial: %i", ret);
			/* ret == 0 is a timeout, otherwise ret is some error */
			return (ret == 0) ? SR_ERR : ret;
		}

		switch (response) {
		case USB_SETTING_SUCCESS:
			sr_spew("%s: Received 0x%x", __func__, (int) response);
			return SR_OK;
		case USB_SETTING_ERROR:
			sr_warn("%s: USB_SETTING_ERROR while processing 0x%x", __func__,
				(int) cmd[PACKET_OPCODE]);
			return SR_ERR;
		case PORTA_SAMPLES:
			/* fallthrough */
		case PORTB_SAMPLES:
			/* unexpected sample packet. Just ignore the rest of it
			   and try again */
			sr_spew("Ignoreing unexpected sample packet");
			ret = serial_read_nonblocking(serial, junk_buffer, NUM_SAMPLES + 1);
			get_counter(&devc->recv_counter);
			if (ret != NUM_SAMPLES + 1) {
				sr_warn("Error reading from serial: %i", ret);
				/* ret == 0 is a timeout, otherwise ret is some error */
				return (ret == 0) ? SR_ERR : ret;
			}
			continue;
		default:
			sr_warn("%s: Unknown response 0x%x while processing 0x%x",
				__func__, (int) response, (int) cmd[0]);
			return SR_ERR;
		}
	}
}

static int ct_short_cmd(struct dev_context *devc,
		struct sr_serial_dev_inst *serial, char cmd)
{

	return ct_cmd(devc, serial, &cmd, sizeof(cmd));
}

/**
 * Turn off interrogizer LED
 *
 * @param devc Device context
 * @param serial Previously initialized serial port structure
 *
 * @retval SR_OK on success
 * @retval other Error. See src/serial.c
 *
 * @private
 */
SR_PRIV int ct_led_off(struct dev_context *devc,
		struct sr_serial_dev_inst *serial)
{
	return ct_short_cmd(devc, serial, CMD_LED_SET_OFF);
}

/**
 * Turn on interrogizer LED, setting it to colour
 *
 * @param devc Device context
 * @param serial Previously initialized serial port structure
 * @param[in] colour The colour to set the LED to
 *
 * @retval SR_OK on success
 * @retval other Error. See src/serial.c
 *
 * @private
 */
SR_PRIV int ct_led_colour(struct dev_context *devc,
		struct sr_serial_dev_inst *serial,
		enum ct_colour colour)
{
	char cmd;

	switch (colour) {
	case CT_RED:
		cmd = CMD_LED_SET_RED;
		break;
	case CT_GREEN:
		cmd = CMD_LED_SET_GREEN;
		break;
	case CT_BLUE:
		cmd = CMD_LED_SET_BLUE;
		break;
	default:
		return SR_ERR;
	}

	return ct_short_cmd(devc, serial, cmd);
}

/**
 * Send ping command to check interrogizer is alive
 *
 * @param devc Device context
 * @param serial Previously initialized serial port structure
 *
 * @retval SR_OK on success
 * @retval other Error. See src/serial.c
 *
 * @private
 */
SR_PRIV int ct_ping(struct dev_context *devc,
		struct sr_serial_dev_inst *serial)
{
	return ct_short_cmd(devc, serial, CMD_PING);
}

/**
 * Tell interrogizer to set its sample rate
 *
 * @param devc Device context
 * @param serial Previously initialized serial port structure
 * @param[in] sample_rate sample rate to set
 *
 * @retval SR_OK on success
 * @retval SR_ERR_SAMPLERATE invalid sample rate
 * @retval other Error. See src/serial.c
 *
 * @private
 */
SR_PRIV int ct_set_sample_rate(struct dev_context *devc,
		struct sr_serial_dev_inst *serial,
		uint64_t sample_rate)
{
	char cmd[2];

	cmd[0] = CMD_SET_SAMPLE_RATE;
	switch (sample_rate) {
	case SR_KHZ(200):
		cmd[1] = SAMPLE_200KHZ;
		break;
	case SR_KHZ(100):
		cmd[1] = SAMPLE_100KHZ;
		break;
	case SR_KHZ(50):
		cmd[1] = SAMPLE_50KHZ;
		break;
	case SR_KHZ(20):
		cmd[1] = SAMPLE_20KHZ;
		break;
	case SR_KHZ(10):
		cmd[1] = SAMPLE_10KHZ;
		break;
	case SR_KHZ(5):
		cmd[1] = SAMPLE_5KHZ;
		break;
	case SR_KHZ(2):
		cmd[1] = SAMPLE_2KHZ;
		break;
	case SR_KHZ(1):
		cmd[1] = SAMPLE_1KHZ;
		break;
	case SR_HZ(500):
		cmd[1] = SAMPLE_500HZ;
		break;
	case SR_HZ(200):
		cmd[1] = SAMPLE_200HZ;
		break;
	case SR_HZ(100):
		cmd[1] = SAMPLE_100HZ;
		break;
	case SR_HZ(50):
		cmd[1] = SAMPLE_50HZ;
		break;
	case SR_HZ(20):
		cmd[1] = SAMPLE_20HZ;
		break;
	case SR_HZ(10):
		cmd[1] = SAMPLE_10HZ;
		break;
	case SR_HZ(5):
		cmd[1] = SAMPLE_5HZ;
		break;
	case SR_HZ(2):
		cmd[1] = SAMPLE_2HZ;
		break;
	case SR_HZ(1):
		cmd[1] = SAMPLE_1HZ;
		break;
	default:
		return SR_ERR_SAMPLERATE;
	}

	return ct_cmd(devc, serial, cmd, ARRAY_SIZE(cmd));
}

/**
 * Tell interrogizer to set its sample limit
 *
 * @param devc Device context
 * @param serial Previously initialized serial port structure
 * @param[in] capture_length sample limit to set
 *
 * @retval SR_OK on success
 * @retval SR_ERR_ARG invalid sample limit
 * @retval other Error. See src/serial.c
 *
 * @private
 */
SR_PRIV int ct_set_capture_length(struct dev_context *devc,
		struct sr_serial_dev_inst *serial,
		uint64_t capture_length)
{
	char cmd[9];
	unsigned i;

	cmd[0] = CMD_SET_CAPTURE_LENGTH;

	/* big endian */
	for (i = 1; i <= 8; i++) {
		cmd[i] = (capture_length >> (8 * (8 - i))) & 0xff;
	}

	return ct_cmd(devc, serial, cmd, ARRAY_SIZE(cmd));
}

/**
 * Enable/disable interrogizer channels
 *
 * @param devc Device context
 * @param serial Previously initialized serial port structure
 * @param[in] porta Port A mask (set bits mean enabled channels)
 * @param[in] portb Port B mask (set bits mean enabled channels)
 *
 * @retval SR_OK on success
 * @retval other Error. See src/serial.c
 *
 * @private
 */
SR_PRIV int ct_sel_channels(struct dev_context *devc,
		struct sr_serial_dev_inst *serial,
		uint8_t porta, uint8_t portb)
{
	char cmd[3];

	cmd[0] = CMD_SEL_CHANNELS;
	cmd[1] = porta;
	cmd[2] = portb;

	return ct_cmd(devc, serial, cmd, ARRAY_SIZE(cmd));
}

/**
 * Tell interrogizer to begin acquisition
 *
 * @param devc Device context
 * @param serial Previously initialized serial port structure
 *
 * @retval SR_OK on success
 * @retval other Error. See src/serial.c
 *
 * @private
 */
SR_PRIV int ct_begin_acq(struct dev_context *devc,
		struct sr_serial_dev_inst *serial)
{
	return ct_short_cmd(devc, serial, CMD_BEGIN_ACQ);
}

/**
 * Tell interrogizer to end acquisition
 *
 * @param devc Device context
 * @param serial Previously initialized serial port structure
 *
 * @retval SR_OK on success
 * @retval other Error. See src/serial.c
 *
 * @private
 */
SR_PRIV int ct_end_acq(struct dev_context *devc,
		struct sr_serial_dev_inst *serial)
{
	return ct_short_cmd(devc, serial, CMD_END_ACQ);
}

/**
 * Tell interrogizer to do a soft reset
 *
 * @param devc Device context
 * @param serial Previously intitialized serial port structure
 *
 * @retval SR_OK on success
 * @retval other Error
 *
 * @private
 */
SR_PRIV int ct_reset(struct dev_context *devc, struct sr_serial_dev_inst *serial)
{
	return ct_short_cmd(devc, serial, CMD_RESET);
}

#define SP_ASSERT(result)              \
	do {                           \
		if (result != SP_OK)   \
			goto out;      \
	} while (0)                    \

/* set the serial port up. Basically we want to turn off everything and get
   raw bytes */
static int ct_setup_serial(struct sr_serial_dev_inst *serial)
{
	int ret;
	struct sp_port *port;
	struct sp_port_config *conf;

	port = serial->sp_data;

	ret = SR_ERR;
	if (sp_new_config(&conf) != SP_OK) {
		return ret;
	}

	/* set a valid baudrate (CDC ignores baud rate anyway) */
	SP_ASSERT(sp_set_config_baudrate(conf, 115200));

	/* 8 bits - ignored too? */
	SP_ASSERT(sp_set_config_bits(conf, 8));

	/* ignore CTS pin */
	SP_ASSERT(sp_set_config_cts(conf, SP_CTS_IGNORE));

	/* ignore DSR pin */
	SP_ASSERT(sp_set_config_dsr(conf, SP_DSR_IGNORE));

	/* ignore DTR pin */
	SP_ASSERT(sp_set_config_dtr(conf, SP_DTR_OFF));

	/* no flow control */
	SP_ASSERT(sp_set_config_flowcontrol(conf, SP_FLOWCONTROL_NONE));

	/* no parity bits */
	SP_ASSERT(sp_set_config_parity(conf, SP_PARITY_NONE));

	/* Ignore RTS pin */
	SP_ASSERT(sp_set_config_rts(conf, SP_RTS_OFF));

	/* No stop bits */
	SP_ASSERT(sp_set_config_stopbits(conf, -1));

	/* Disable XON/XOFF */
	SP_ASSERT(sp_set_config_xon_xoff(conf, SP_XONXOFF_DISABLED));

	SP_ASSERT(sp_set_config(port, conf));

	ret = SR_OK;
out:
	sp_free_config(conf);
	return ret;
}

/**
 * Open and set up a serial port for interrogizer
 *
 * @param devc Device context
 * @param serial Previously initialized serial port structure.
 *
 * @retval SR_OK Success
 * @retval SR_ERR Failure
 *
 * @private
 */
SR_PRIV int ct_open(struct dev_context *devc,
		struct sr_serial_dev_inst *serial)
{
	int ret;

	ret = serial_open(serial, SERIAL_RDWR);
	if (ret != SR_OK) {
		sr_warn("Failed to open %s", serial->port);
		return ret;
	}

	ret = ct_setup_serial(serial);
	if (ret != SR_OK) {
		sr_warn("Failed to set up %s", serial->port);
		return ret;
	}

	/* reset interrogizer so we don't have any state hanging around from
	   previous sessions */
	ret = ct_reset(devc, serial);
	if (ret != SR_OK) {
		sr_warn("Failed to reset interrogizer");
		return ret;
	}

	return SR_OK;
}

/**
 * Instantiate a new interrogizer device context
 *
 * @retval NULL not enough memory to allocate device context
 * @retval other pointer to device context. Free with g_free
 *
 * @private
 */
SR_PRIV struct dev_context *ct_dev_new(void)
{
	struct dev_context *devc;

	devc = g_try_malloc0(sizeof(*devc));
	if (!devc) {
		return NULL;
	}

	devc->sample_rate = DEFAULT_SAMPLE_RATE;
	devc->limit_samples = DEFAULT_LIMIT_SAMPLES;
	devc->porta_mask = DEFAULT_PORTA_MASK;
	devc->portb_mask = DEFAULT_PORTB_MASK;

	devc->porta_queue = g_queue_new();
	devc->portb_queue = g_queue_new();

	devc->send_counter = 0;
	devc->recv_counter = 0;

	return devc;
}

/**
 * Free an interrogizer device context
 *
 * This is never actually called because device contexts are never freed
 *
 * @param devc Device context to free
 *
 * @private
 */
SR_PRIV void ct_dev_free(struct dev_context *devc)
{
	g_queue_free_full(devc->porta_queue, g_free);
	g_queue_free_full(devc->portb_queue, g_free);
	g_free(devc);
}

static gboolean u64_in_array(uint64_t u64, const uint64_t array[], size_t len)
{
	size_t i;
	gboolean found;

	found = FALSE;
	for (i = 0; i < len; i ++) {
		if (array[i] == u64) {
			found = TRUE;
		}
	}

	return found;
}

/**
 * Check if a proposed sample rate is supported
 *
 * @param devc Device context
 * @param[in] sample_rate The sample rate to check
 *
 * @retval TRUE The sample rate is valid
 * @retval FALSE The sample rate is invalid
 *
 * @private
 */
SR_PRIV gboolean ct_valid_sample_rate(uint64_t sample_rate)
{
	return u64_in_array(sample_rate, samplerates, ARRAY_SIZE(samplerates));
}

/**
 * Check if a proposed sample limit is supported
 *
 * @param devc Device context
 * @param[in] sample_limit The sample limit to check
 *
 * @retval TRUE The sample limit is valid
 * @retval FALSE The sample limit is invalid
 *
 * @private
 */
SR_PRIV gboolean ct_valid_sample_limit(uint64_t sample_limit)
{
	return sample_limit >= MIN_NUM_SAMPLES && sample_limit <= MAX_NUM_SAMPLES;
}

/**
 * Stop sample acquisition on interrogizer
 *
 * @param sdi Sigrok device
 *
 * @retval SR_OK Success
 * @retval SR_ERR* Failure
 *
 * @private
 */
SR_PRIV int ct_acquisition_stop(struct sr_dev_inst *sdi)
{
	int ret, retries;
	struct sr_serial_dev_inst *serial;
	struct dev_context *devc = sdi->priv;

	serial = sdi->conn;

	/* There's a race here where the device might send more samples
	 * before it receives CMD_END_ACQ. We would then read the samples
	 * instead of the ack for CMD_END_ACQ and report failure.
	 * Get around this by making two attempts - if we hit the race
	 * on the first time then the device got the command but we lost the
	 * ack. On the second attempt the device won't be sending any samples
	 * so the race can't happen.
	 */
	retries = 2;
	do {
		retries -= 1;

		ret = ct_end_acq(devc, serial);
		if (ret == SR_OK) {
			break;
		} else if (ret != SR_ERR) {
			/* this isn't a nack error */
			break;
		}
	} while (retries > 0);
	if (ret != SR_OK) {
		sr_warn("ct_end_acq failed");
		return ret;
	}

	/* change LED to indicate we're stopping */
	ret = ct_led_colour(devc, serial, CT_GREEN);
	if (ret != SR_OK) {
		sr_warn("Setting LED green failed");
		return ret;
	}

	ret = serial_source_remove(sdi->session, serial);
	if (ret != SR_OK) {
		sr_warn("serial_source_remove failed");
		return ret;
	}

	return std_session_send_df_end(sdi);
}
