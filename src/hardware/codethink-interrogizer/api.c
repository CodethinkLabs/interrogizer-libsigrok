/*
 * This file is part of the libsigrok project.
 *
 * Copyright (C) 2020 Tom Eccles <tom.eccles@codethink.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "protocol.h"

static const uint32_t scanopts[] = {
	SR_CONF_CONN,
	SR_CONF_SERIALCOMM,
};

static const uint32_t drvopts[] = {
	SR_CONF_LOGIC_ANALYZER,
};

static const uint32_t devopts[] = {
	SR_CONF_SAMPLERATE | SR_CONF_GET | SR_CONF_SET | SR_CONF_LIST,
	SR_CONF_LIMIT_SAMPLES | SR_CONF_GET | SR_CONF_SET | SR_CONF_LIST,
	SR_CONF_CONTINUOUS | SR_CONF_GET | SR_CONF_SET | SR_CONF_LIST,
};

static const char *channel_names[] = {
	/* order chosen to match PCB */
	"PB8", "PB9", "PB10", "PB11", "PB12", "PB13", "PB14", "PB15",
	"PA0", "PA1", "PA2",  "PA3",  "PA4",  "PA5",  "PA6",  "PA7",
};

static struct sr_dev_driver codethink_interrogizer_driver_info;

static GSList *scan(struct sr_dev_driver *di, GSList *options)
{
	struct sr_dev_inst *sdi;
	struct sr_config *src;
	struct sr_serial_dev_inst *serial;
	struct dev_context *devc;
	GSList *l;
	const char *conn;
	const char *serialcomm;
	size_t i;

	/* Get the manditory conn option e.g. conn=/dev/ttyACM0 */
	conn = NULL;
	serialcomm = NULL;
	for (l = options; l; l = l->next) {
		src = l->data;
		switch (src->key) {
		case SR_CONF_CONN:
			conn = g_variant_get_string(src->data, NULL);
			break;
		case SR_CONF_SERIALCOMM:
			/*
			 * ignore: baud rate and similar serial settings are
			 * not used by ttyACM. We allow but ignore this option
			 * because the pulseview GUI makes it look like this
			 * ought to work
			 */
			serialcomm = g_variant_get_string(src->data, NULL);
			if (serialcomm && serialcomm[0] != '\0') {
				sr_warn("Ignoring serialcomm option '%s' because it is not supported by ttyACM",
					serialcomm);
			}
			break;
		}
	}
	if (!conn)
		return NULL;

	/* 115200/8n1 is ignored by ttyACM. We just have to give it something
	   sensible */
	serial = sr_serial_dev_inst_new(conn, "115200/8n1");
	if (!serial)
		return NULL;

	devc = ct_dev_new();
	if (!devc) {
		sr_warn("No memory");
		return NULL;
	}

	/* send a ping command to check it is an interrogizer */
	if (ct_open(devc, serial) != SR_OK) {
		ct_dev_free(devc);
		return NULL;
	}
	if (ct_ping(devc, serial) != SR_OK) {
		ct_dev_free(devc);
		return NULL;
	}

	/* set LED to red */
	i = ct_led_colour(devc, serial, CT_RED);
	serial_close(serial);
	if (i != SR_OK) {
		ct_dev_free(devc);
		sr_warn("Device didn't respond to LED colour change");
		return NULL;
	}

	sdi = g_malloc0(sizeof(*sdi));
	sdi->status = SR_ST_INACTIVE;
	sdi->vendor = g_strdup("Codethink");
	sdi->model = g_strdup("Interrogizer");
	sdi->version = g_strdup("v0.1");

	for (i = 0; i < ARRAY_SIZE(channel_names); i++)
		sr_channel_new(sdi, i, SR_CHANNEL_LOGIC, TRUE,
				channel_names[i]);
	/* TODO: analog channels */
	sdi->inst_type = SR_INST_SERIAL;
	sdi->conn = serial;
	sdi->priv = devc;

	return std_scan_complete(di, g_slist_append(NULL, sdi));
}

static int dev_open(struct sr_dev_inst *sdi)
{
	int ret;
	struct sr_serial_dev_inst *serial;
	struct dev_context *devc = sdi->priv;

	serial = sdi->conn;

	ret = ct_open(devc, serial);
	if (ret != SR_OK)
		return ret;

	/* indicate we're open and ready to go */
	return ct_led_colour(devc, serial, CT_GREEN);
}

static int dev_close(struct sr_dev_inst *sdi)
{
	struct sr_serial_dev_inst *serial;
	struct dev_context *devc = sdi->priv;

	serial = sdi->conn;

	/* ignore return value so we never skip closing */
	ct_led_colour(devc, serial, CT_RED);

	return std_serial_dev_close(sdi);
}

static int config_get(uint32_t key, GVariant **data,
	const struct sr_dev_inst *sdi, const struct sr_channel_group *cg)
{
	int ret;
	struct dev_context *devc;

	(void)cg;

	devc = sdi->priv;

	ret = SR_OK;
	switch (key) {
	case SR_CONF_SAMPLERATE:
		*data = g_variant_new_uint64(devc->sample_rate);
		break;
	case SR_CONF_LIMIT_SAMPLES:
		*data = g_variant_new_uint64(devc->limit_samples);
		break;
	default:
		return SR_ERR_NA;
	}

	return ret;
}

static int config_set(uint32_t key, GVariant *data,
	const struct sr_dev_inst *sdi, const struct sr_channel_group *cg)
{
	int ret;
	struct dev_context *devc;
	uint64_t tmp_u64;

	(void)cg;

	devc = sdi->priv;

	ret = SR_OK;
	switch (key) {
	case SR_CONF_SAMPLERATE:
		tmp_u64 = g_variant_get_uint64(data);
		if (ct_valid_sample_rate(tmp_u64)) {
			devc->sample_rate = tmp_u64;
		} else {
			ret = SR_ERR_SAMPLERATE;
		}
		break;
	case SR_CONF_LIMIT_SAMPLES:
		tmp_u64 = g_variant_get_uint64(data);
		if (ct_valid_sample_limit(tmp_u64)) {
			devc->limit_samples = tmp_u64;
		} else {
			ret = SR_ERR_ARG;
		}
		break;
	default:
		ret = SR_ERR_NA;
	}

	return ret;
}

static int config_list(uint32_t key, GVariant **data,
	const struct sr_dev_inst *sdi, const struct sr_channel_group *cg)
{
	int ret;

	ret = SR_OK;
	switch (key) {
	case SR_CONF_SCAN_OPTIONS:
		/* fallthrough */
	case SR_CONF_DEVICE_OPTIONS:
		return STD_CONFIG_LIST(key, data, sdi, cg, scanopts, drvopts, devopts);
	case SR_CONF_SAMPLERATE:
		*data = std_gvar_samplerates(ARRAY_AND_SIZE(samplerates));
		break;
	case SR_CONF_LIMIT_SAMPLES:
		*data = std_gvar_tuple_u64(MIN_NUM_SAMPLES, MAX_NUM_SAMPLES);
		break;
	default:
		return SR_ERR_NA;
	}

	return ret;
}

static int dev_acquisition_start(const struct sr_dev_inst *sdi)
{
	struct dev_context *devc;
	struct sr_serial_dev_inst *serial;
	int ret;
	GSList *channels;
	struct sr_channel *channel;

	devc = sdi->priv;
	serial = sdi->conn;

	ret = ct_set_sample_rate(devc, serial, devc->sample_rate);
	if (ret != SR_OK) {
		sr_warn("ct_set_sample_rate failed");
		return ret;
	}
	ret = ct_set_capture_length(devc, serial, devc->limit_samples);
	if (ret != SR_OK) {
		sr_warn("ct_set_capture_length failed");
		return ret;
	}

	devc->portb_mask = 0xff;
	devc->porta_mask = 0xff;

	/* check which channels should be disabled */
	for (channels = sdi->channels; channels; channels = channels->next) {
		channel = channels->data;
		if (!channel->enabled) {
			sr_spew("channel '%s' is disabled", channel->name);
			if (channel->index < 8) {
				/* port b */
				devc->portb_mask &= ~(1 << channel->index);
			} else if (channel->index < 16) {
				/* port a */
				devc->porta_mask &= ~(1 << (channel->index - 8));
			}
		}
	}

	ret = ct_sel_channels(devc, serial, devc->porta_mask, devc->portb_mask);
	if (ret != SR_OK) {
		sr_warn("ct_sel_channels failed");
		return ret;
	}
	ret = ct_begin_acq(devc, serial);
	if (ret != SR_OK) {
		sr_warn("ct_begin_acq failed");
		return ret;
	}
	/* change LED to indicate we're acquiring */
	ret = ct_led_colour(devc, serial, CT_BLUE);
	if (ret != SR_OK) {
		sr_warn("ct_led_colour failed");
		return ret;
	}

	/* reset acquisition state */
	devc->num_samples = 0;

	/* send header to our data sink to indicate it is starting */
	std_session_send_df_header(sdi);

	/*
	 * Calculate timeout
	 * This needs to be long enough that we can buffer MAX_PACKET_LEN samples
	 * at a slow sample rate, but if this is too long we don't notice a dead
	 * device.
	 */

	/* the time taken for one sample packet is NUM_SAMPLES/devc->sample_rate */
	double sample_packet_period =
		((double) NUM_SAMPLES)/((double) devc->sample_rate);
	/* wait for two sample packet periods before reporting a timeout */
	double timeout_secs = 2 * sample_packet_period;
	int timeout_ms = (int) (1000 * timeout_secs);
	if (timeout_ms < 1) {
		timeout_ms = 1;
	}
	ret = serial_source_add(sdi->session, serial, G_IO_IN, timeout_ms,
			ct_receive_data, (struct sr_dev_inst *)sdi);
	if (ret != SR_OK) {
		sr_warn("serial_source_add failed");
		return ret;
	}

	return SR_OK;
}

static int dev_acquisition_stop(struct sr_dev_inst *sdi)
{
	return ct_acquisition_stop(sdi);
}


static struct sr_dev_driver codethink_interrogizer_driver_info = {
	.name = "codethink-interrogizer",
	.longname = "Codethink Interrogizer",
	.api_version = 1,
	.init = std_init,
	.cleanup = std_cleanup,
	.scan = scan,
	.dev_list = std_dev_list,
	.dev_clear = std_dev_clear,
	.config_get = config_get,
	.config_set = config_set,
	.config_list = config_list,
	.dev_open = dev_open,
	.dev_close = dev_close,
	.dev_acquisition_start = dev_acquisition_start,
	.dev_acquisition_stop = dev_acquisition_stop,
	.context = NULL,
};
SR_REGISTER_DEV_DRIVER(codethink_interrogizer_driver_info);
