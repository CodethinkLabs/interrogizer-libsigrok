/*
 * This file is part of the libsigrok project.
 *
 * Copyright (C) 2020 Tom Eccles <tom.eccles@codethink.co.uk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBSIGROK_HARDWARE_CODETHINK_INTERROGIZER_PROTOCOL_H
#define LIBSIGROK_HARDWARE_CODETHINK_INTERROGIZER_PROTOCOL_H

#include <stdint.h>
#include <glib.h>
#include <libsigrok/libsigrok.h>
#include "libsigrok-internal.h"

#include "usb_protocol.h"

#define LOG_PREFIX "codethink-interrogizer"

struct dev_context {
	uint64_t sample_rate;
	uint64_t limit_samples;

	/* 1 bit per pin, 1 means the channel is enabled */
	uint8_t porta_mask;
	uint8_t portb_mask;

	uint64_t num_samples;
	GQueue *porta_queue;
	GQueue *portb_queue;

	uint8_t send_counter;
	uint8_t recv_counter;
};

#define MIN_NUM_SAMPLES 100
#define MAX_NUM_SAMPLES 10000000000 /* 10G */

#define NUM_SAMPLE_RATES 17
extern const uint64_t samplerates[NUM_SAMPLE_RATES];

/* largest sample rate where all channels can be used */
#define DEFAULT_SAMPLE_RATE SR_KHZ(200)
#define DEFAULT_LIMIT_SAMPLES MAX_NUM_SAMPLES
/* all channels enabled */
#define DEFAULT_PORTA_MASK 0xff
#define DEFAULT_PORTB_MASK 0xff

SR_PRIV int ct_receive_data(int fd, int revents, void *cb_data);

SR_PRIV struct dev_context *ct_dev_new(void);
SR_PRIV void ct_dev_free(struct dev_context *devc);

SR_PRIV int ct_open(struct dev_context *devc, struct sr_serial_dev_inst *serial);

SR_PRIV gboolean ct_valid_sample_rate(uint64_t sample_rate);
SR_PRIV gboolean ct_valid_sample_limit(uint64_t sample_limit);

/**
 * Interrogizer LED colours
 */
SR_PRIV enum ct_colour {
	CT_RED = 10000, /**< Red */
	CT_GREEN = 20000, /**< Green */
	CT_BLUE = 30000, /**< Blue */
};

SR_PRIV int ct_led_off(struct dev_context *devc,
		struct sr_serial_dev_inst *serial);
SR_PRIV int ct_led_colour(struct dev_context *devc,
		struct sr_serial_dev_inst *serial,
		enum ct_colour colour);
SR_PRIV int ct_ping(struct dev_context *devc,
		struct sr_serial_dev_inst *serial);
SR_PRIV int ct_set_sample_rate(struct dev_context *devc,
		struct sr_serial_dev_inst *serial,
		uint64_t sample_rate);
SR_PRIV int ct_set_capture_length(struct dev_context *devc,
		struct sr_serial_dev_inst *serial,
		uint64_t capture_length);
SR_PRIV int ct_sel_channels(struct dev_context *devc,
		struct sr_serial_dev_inst *serial,
		uint8_t porta, uint8_t portb);
SR_PRIV int ct_begin_acq(struct dev_context *devc,
		struct sr_serial_dev_inst *serial);
SR_PRIV int ct_end_acq(struct dev_context *devc,
		struct sr_serial_dev_inst *serial);
SR_PRIV int ct_reset(struct dev_context *devc,
		struct sr_serial_dev_inst *serial);

SR_PRIV int ct_acquisition_stop(struct sr_dev_inst *sdi);

#endif
